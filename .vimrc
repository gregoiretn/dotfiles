" On utilise un format unix et utf-8
set fileformat=unix
set encoding=utf-8

" Désactive la compatibilité avec vi
set nocompatible

" Pour avoir toute les couleurs
set t_Co=256

" Afficher les numéros de ligne
set nu

" Afficher les parenthèses correspondantes
set showmatch

" Supprime les tabulations et met des espaces
set expandtab
" Modifier la taille des tabulations
set tabstop =2
set shiftwidth =2
set softtabstop =2

" Requis par Vundle
filetype off
set rtp+=~/.vim/bundle/Vundle.vim/
call vundle#rc()
Bundle 'gmarik/vundle'

" My Bundles here:
Plugin 'bling/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'tpope/vim-fugitive' " Pour avoir git dans airline
Plugin 'othree/yajs.vim'
Plugin 'groenewege/vim-less'
Plugin 'othree/html5.vim'
Plugin 'hail2u/vim-css3-syntax'
Plugin 'leafgarland/typescript-vim'
Plugin 'scrooloose/syntastic'
Plugin 'editorconfig/editorconfig-vim'

" Activer la coloration et l'indentation
syn on 
set syntax =on
filetype indent plugin on

" Configuration de Airline
set laststatus=2 " pour apparaître tout le temps
let g:airline_powerline_fonts = 1 " On utilise les symboles powerline
let g:airline_section_z = '%l/%L:%c' " Affichage n° de ligne
let g:airline_theme='badwolf'
"let g:airline_detect_whitespace=0 " déprécié
let g:airline#extensions#whitespace#enabled=0 " desactiv de la detection des espaces

" Affiche les commande incomplètes
set showcmd

"retour à la ligne automatique des fichier texte
autocmd FileType text setlocal textwidth=78
autocmd FileType azerty setlocal textwidth=78
autocmd FileType kdoc setlocal textwidth=78
autocmd FileType tex setlocal textwidth=78

"Syntastic
" npm i -g eslint
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1
let g:syntastic_javascript_checkers = ['eslint']

let g:syntastic_error_symbol = '❌'
let g:syntastic_style_error_symbol = '⁉️'
let g:syntastic_warning_symbol = '⚠️'
let g:syntastic_style_warning_symbol = '💩'

