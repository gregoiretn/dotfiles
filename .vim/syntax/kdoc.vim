" Vim syntax file
" Language:     Képhâ Document
" Maintainer:   Grégoire Oliveira Silva <gregoire@theo-net.org>
" Filenames:    *.kdoc
" Last Change:  2014 october 29

if exists("b:current_syntax")
  finish
endif

syntax clear
syntax case match
syntax sync minlines=10


" %
syntax match kdocComment /%.*/
" HEADER => /HEADER (possibilter d'échaper avec \)
syntax region kdocPreProc start=/HEADER/ skip=/\\\/HEADER/ end=/\/HEADER/
" TODO => /TODO
syntax region kdocTodo start=/TODO/ skip=/\\\/TODO/ end=/\/TODO/

" \\ retour à la ligne
syntax match kdocRC /\\\\[ \t\n]/

" [[fdf|fdsf]] liens
syntax region kdocLink start=/\[\[/ end=/]]/
" {{fichier}}
syntax region kdocFile start=/{{/ end=/}}/
" <fdf@fdsf.df> couriel
syntax match kdocMail /<.\+@.\+\..\+>/

" Titre 1 (=====)
syntax match kdocTitle1 /.\+\n\s*=\{3,}/
" Titre 2 (-----)
syntax match kdocTitle2 /.\+\n\s*-\{3,}/
" Titre 3 (#  #)
syntax match kdocTitle3 /\s*#.*#/
" Titre 4 (## ##)
syntax match kdocTitle4 /\s*##.*##/
" Titre 5 (### ###)
syntax match kdocTitle5 /\s*###.*###/
" Titre 6 (#### ####)
syntax match kdocTitle6 /\s*####.*####/

syntax match kdocHR /\s*----/

" **gras**
syntax match kdocBold /\*\*.\+\*\*/
" //italique//
syntax match kdocItalic /\/\/.\+\/\//
" __souligné__
syntax match kdocUnderlined /__.\+__/
" _{indice} et ^{exposant}
syntax region kdocSub start=/_{/ skip=/\\}/ end=/}/
syntax region kdocSup start=/\^{/ skip=/\\}/ end=/}/
" -{supprimé}
syntax region kdocDeleted start=/-{/ skip=/\\}/ end=/}/
" bsc{petites majuscules}
syntax region kdocSmallCap start=/bsc{/ skip=/\\}/ end=/}/
" ((notes))
syntax region kdocNote start=/((/ skip=/\\)/ end=/))/
" Les listes
syntax match kdocList "\%(\t\| \{0,4}\)[-*]\%(\s\+\S\)\@="
" > citations
syntax match kdocCitation /^\s*>\+/

" Hébreu
syntax region kdocHebreu start=/he{/ skip=/\\}/ end=/}/
" Grec
syntax region kdocGrec start=/gr{/ skip=/\\}/ end=/}/
" include
syntax region kdocInclude start=/include{/ skip=/\\}/ end=/}/

" Les conversions textuelles
syntax match kdocGrandTiret /[^-]--[^-]/
syntax match kdocTGrandTiret /[^-]---[^-]/
syntax match kdocFD /->/
syntax match kdocFG /<-/
syntax match kdocFGD /<->/
syntax match kdocBFD /=>/
syntax match kdocBFG /<=/
syntax match kdocBFGD /<=>/
syntax match kdocGuillemetsD />>/
syntax match kdocGuillemetsG /<</
syntax match kdocMultiplication /\d+x\d+/
syntax match kdocCopyright /(c)/
syntax match kdocTM /(tm)/
syntax match kdocR /(r)/


" General
highlight link kdocPreProc Error
highlight link kdocTodo Todo
highlight link kdocComment Comment

" Title
highlight link kdocTitle1 Label
highlight link kdocTitle2 Label
highlight link kdocTitle3 Label
highlight link kdocTitle4 Label
highlight link kdocTitle5 Label
highlight link kdocTitle6 Label
highlight link kdocHR Label

" Emph
highlight kdocBold term=bold cterm=bold gui=bold
highlight kdocItalic term=italic cterm=italic gui=italic
highlight kdocUnderlined term=underline cterm=underline gui=underline
highlight link kdocSub PreProc
highlight link kdocSup PreProc
highlight link kdocDeleted PreProc
highlight link kdocSmallCap PreProc

highlight link kdocRC Constant

" à underline
highlight link kdocLink Type 
highlight link kdocMail Type 
highlight link kdocNote Special
highlight link kdocFile Type
highlight link kdocCitation Identifier

highlight link kdocHebreu Special
highlight link kdocGrec Special
highlight link kdocInclude Constant

highlight link kdocList Function
highlight link kdocGrandTiret Function
highlight link kdocTGrandTiret Function
highlight link kdocFD Function
highlight link kdocFG Function
highlight link kdocFGD Function
highlight link kdocBFD Function
highlight link kdocBFG Function
highlight link kdocBFGD Function
highlight link kdocGuillemetsD Function
highlight link kdocGuillemetsG Function
highlight link kdocMultiplication Function
highlight link kdocCopyright Function
highlight link kdocTM Function
highlight link kdocR Function

if !exists('main_syntax')
  let main_syntax = 'kdoc'
endif

let b:current_syntax = "kdoc"
if main_syntax ==# 'kdoc'
  unlet main_syntax
endif

" vim:set sw=2:
