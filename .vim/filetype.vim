augroup filetypedetect
    au BufNewFile,BufRead *.kdoc    setf kdoc
    au BufNewFile,BufRead *.tex     setf tex
augroup END

au BufRead,BufNewFile /etc/nginx/*,/usr/local/nginx/conf/* if &ft == '' | setfiletype nginx | endif

au BufNewFile,BufRead *.ejs set filetype=html

